(ns tbf7.app
  (:require
   [reagent.core :as reagent]
   [cljss.core :as css :refer-macros [defstyles defkeyframes]]
   [quil.core :as q]
   [quil.middleware :as m]))

(defstyles global-css []
  {:padding-left "80px"})

(defn concat-css [& css]
  (clojure.string/join " " (map apply css)))

(defstyles root-css []
  {:overflow-x "hidden"
   :width "100%" :height "auto"
   :border "1px solid transparent"
   :font-family "Krub, \"M+PLUS+1p\", sans-serif"
   :font-weight "300"
   :color "hsla(0, 0%, 80%, 1)"
   :background-color "hsla(0, 0%, 10%, 1)"
   "p" {:margin "0.5em"}})

(defstyles a-css []
  {:text-decoration "underline"
   :color "hsla(120, 50%, 40%, 1)"
   :&:visited {:text-decoration "none"
               :color "hsla(120, 50%, 40%, 1)"
               :filter "blur(1px)"}
   :&:hover {:text-decoration "none"
             :color "hsla(120, 50%, 80%, 1)"}
   :&:focus {:text-decoration "none"
             :color "hsla(120, 50%, 80%, 1)"}})

(defn quil-init [w h]
  (fn []
    (q/color-mode :hsb)
    (q/background 10)
    (q/stroke 0 0 0 0)
    {:width w :height h
     :ps (for [n (range 100)]
           {:x (q/random w)
            :y (q/random h)
            :s (+ (q/random 50))
            :c (q/random 255)
            :a 0
            :speed (+ 5 (q/random 10))})}))

(defn quil-update [s]
  {:width (:width s)
   :height (:height s)
   :ps (map (fn [p]
              (let [new-x (+ (:x p) (+ 0.4 (* 3 (/ (:x p) (:width s)) (:speed p))))]
                (if (> new-x (/ (:width s) 2))
                  {:x 0 :y (q/random (:height s))
                   :c (q/random 255) :a 0 :s (+ (q/random 50))
                   :speed (+ 3 (q/random 15))}
                  {:x new-x :y (:y p)
                   :c (:c p) :a (:x p)
                   :s (:s p)
                   :speed (:speed p)})))
            (:ps s))})

(defn quil-draw [s]
  (q/background 0 0 20 50)
  (doseq [p (:ps s)]
    (q/text-size (+ 15 (:s p)))
    (q/fill (:c p) 100 255 (:a p))
    (q/text "(" (- (/ (:width s) 2) (:x p) 5) (:y p))
    (q/text ")" (+ (/ (:width s) 2) (:x p) 5) (:y p))))

(defstyles bg-css []
  {:width "100%"
   :height "100%"
   :position "fixed"
   :z-index "0"
   :filter "blur(2px)"})

(defn site-bg []
  [(reagent/create-class
    {:component-did-mount
     (fn [component]
       (let [node (reagent/dom-node component)
             width (.-innerWidth js/window)
             height (.-innerHeight js/window)]
         (q/sketch
          :host node
          :setup (quil-init width height)
          :update quil-update
          :draw quil-draw
          :size [width height]
          :middleware [m/fun-mode])))
     :render
     (fn [component]
       [:div {:class (bg-css)}])})])

(defstyles h2-css []
  {:&:before {:content "ph> (describe  "
              :font-size "0.5em"
              :color "hsla(140, 0%, 70%, 1)"}
   :&:after {:content ")"
             :font-size "0.5em"
             :color "hsla(140, 0%, 70%, 1)"}
;;   :width "600px"
   :padding "5px"
   :margin-top "2em"
   :font-weight "600"
   :font-size "35px"
   :color "hsla(120, 100%, 95%, 1)"
   :border-style "none none none none"
   :border-color "hsla(120, 80%, 60%, 0.1)"
   :border-width "1px"
   :backdrop-filter "blur(3px)"})

(defstyles h3-css []
  {:&:before {:content ";; "
              :font-size "1.3em"
              :color "hsla(120, 80%, 60%, 0.5)"}
   :margin-top "1em"
   :font-size "20px"})

(defstyles h4-css []
  {:&:before {:content ";;"
              :padding-right "5px"
              :filter "blur(1px)"
              :font-size "1.3em"
              :color "hsla(120, 80%, 60%, 0.7)"}
   :font-size "15px"})

(defn h [n body]
  (cond (= n 2) [:h2 {:class (h2-css)} body]
        (= n 3) [:h3 {:class (h3-css)} body]
        (= n 4) [:h4 {:class (h4-css)} body]))

(defstyles header-css []
  {:position "relative"
   :z-index "10"
   :height "150px"
   :margin "10px"
   :padding-top "100px"
   :color "hsla(0, 0%, 100%, 0.8)"
   :background-color "hsla(140, 30%, 50%, 1)"
   :background-image "url(./img/header-bg.png)"
   :border-radius "15px"
   :text-shadow "5px 5px 5px rgba(0, 0, 0, 0.3)"
   :text-align :ceter})

(defstyles paren-holic-css []
  {:font-family "Asul, sans-serif"})

(defstyles header-paren-holic-css []
  {:font-size "70px"})

(defstyles header-at-css []
  {:margin "10px"
   :font-size "30px"
   :color "hsla(0, 0%, 100%, 0.6)"})

(defstyles header-tbf7-css []
  {:font-size "35px"
   :color "hsla(0, 0%, 100%, 0.8)"})

(defn site-header []
  [:header {:class (concat-css global-css header-css)}
   [:css-doodle
    [:h1
     "("
     [:span {:class (concat-css header-paren-holic-css paren-holic-css)}
      "paren-holic"]
     [:span {:class (header-at-css)} "@"]
     [:span {:class (header-tbf7-css)} "技術書典7"]
     ")"]]])

(defstyles site-about-css []
  {:position "relative"
   :z-index "10"
   :width "800px"})

(defn site-about []
  [:section {:class (concat-css global-css site-about-css)}
   (h 2 "paren-holic")
   [:article
    [:p
     "括弧大好き集団です。"
     "日々括弧を開いたり括弧を閉じたりするなど"
     "括弧が好きな人たちが集まって"
     "括弧について話し合ったりしています。"]
    [:p
     "技術書典では括弧についての本を書いて頒布しています。"]]
   [h 3 "サークル配置"]
   [:p
    "サークル配置は"
    [:strong "し08D (2階Dホール)"]
    "です！！！"]
   [:img {:src "./img/circle-layout-2f.png" :width "800"}]])

(defstyles site-book-list-css []
  {:position "relative"
   :z-index "10"
   :width "800px"})

(defn site-book-list []
  [:section {:class (concat-css global-css site-book-list-css)}
    (h 2 "技術書典7の頒布物")
    (h 3 "新刊")
    [:ul
     [:li "Clojureに入門したら知っておきたい5つのこと"]
     [:li "Lispとコンピュータ音楽 Vol.1 - Common Lisp Music"]]
    (h 3 "既刊")
    [:ul
     [:li "3つのLisp 3つの世界"
      [:ul [:li "paren-holicが技術書典6で頒布した、Lispの世界をさっと渉猟できる本です。"]
           [:li [:a {:class (a-css) :href "https://techbookfest.org/event/tbf06/circle/44710001"}
                 "技術書典6のサークルページはこちら。"]]
           [:li [:a {:class (a-css) :href "https://paren-holic.booth.pm/items/1317263"}
                 "boothで電子版を購入できます！"]]]]
     [:li "Survival Common Lisp - 現代Lisperたちの生存プログラミング術"
      " (" [:strong "委託"] ")"
      [:ul [:li "Common Lisp集団「clfreaks」が技術書典6にて頒布した本です。"]
           [:li [:a {:class (a-css) :href "https://techbookfest.org/event/tbf06/circle/38170002"}
                 "技術書典6のサークルページはこちら。"]]
           [:li [:a {:class (a-css) :href "https://clfreaks.booth.pm/items/1300098"}
                 "boothで電子版を購入できます！"]]]]]])

(defstyles book-details-css []
  {})

(defstyles table-css []
  {:margin "10px 20px"
   :color "hsla(120, 30%, 100%, 0.7)"
   :border-collapse "collapse"
   "th, td" {:text-align "left"
             :font-weight "400"
             :border "solid 1px hsla(0, 0%, 100%, 0.3)"}})

(defn book-detail [book-info]
  [:section (:class (concat-css global-css book-details-css))
   (h 3 (:title book-info))
   [:section
    (h 4 "本の概要")
    ;; [:article "ここに表紙 + サンプルページ3つくらい"]
    [:table {:class (table-css)}
     [:tbody
      [:tr [:th "著者"] [:td (:author book-info)]]
      [:tr [:th "判型"] [:td (:size book-info)]]
      [:tr [:th "ページ数"] [:td (:pages book-info)]]]]]
   [:article
    (h 4 "本の内容")
    (:details book-info)]])

(defstyles site-book-details-css []
  {:position "relative"
   :z-index "10"
   :width "800px"})

(defn site-book-details []
  [:section {:class (concat-css global-css site-book-details-css)}
   (h 2 "新刊")
   (book-detail
    {:title "Clojureに入門したら知っておきたい5つのこと"
     :author [:a {:class (a-css) :href "https://twitter.com/lagenorhynque"} "@lagenorhynque"]
     ;; :thumbnails ["ここに表紙 + サンプルページ3つくらい"]
     :size "B5版"
     :pages "44ページ"
     :details [:div
               [:p "中級Clojurianになるために必要な基礎知識を学ぶ本。"]
               [:p "目次:"]
               [:ul
                [:li
                 "基礎理解編"
                 [:ul
                  [:li "1. sequences & data structures"]
                  [:li "2. Symbol, Var, Namespace"]]]
                [:li "実用機能編"
                 [:ul
                  [:li "3. destructuring"]
                  [:li "4. threading macros"]
                  [:li "5. clojure.spec"]]]]]})
   (book-detail
    {:title "Lispとコンピュータ音楽 Vol.1 - Common Lisp Music"
     :author [:a {:class (a-css) :href "https://twitter.com/sin_clav"} "@t-sin"]
     ;; :thumbnails ["ここに表紙 + サンプルページ3つくらい"]
     :size "B5版"
     :pages "46ページ"
     :details [:div
               [:p
                "機械によって音楽を作成・演奏する試みは1900年ごろから存在した。"
                "機械を楽器として見たときの、音の生成の探究。"
                "あるいは機械を自動化機構として見たときの、作曲プロセスの自動化・記述の研究。"
                "それらの試みの結果さまざまなソフトウェアが誕生し、"
                "また音の生成や音楽の記述専用の言語がつくられた。"
                "こんな流れの中で、われらが愛しきLispはどのような役割を演じてきたのか。"]
               [:p
                "……という疑問を解決するべく調査してみる本のその1。"
                "存外に数が多かったのと作業を重くしたくなかったのとで"
                "とりあえず軽い読み物シリーズとして調べていくことにしたもの。"]
               [:p
                "今回はCommon Lisp Musicという音合成ソフトウェアとその周辺のことを書きます。"]]})])

(defstyles site-footer-css []
  {:position "relative"
   :z-index "10"
   :margin "3em 10px 10px 10px"
   :border-radius "15px"
   :padding-top "10px" :padding-bottom "10px"
   :color "hsla(0, 0%, 100%, 1)"
   :background-color "hsla(140, 30%, 30%, 1)"})

(defstyles footer-p-css []
  {:margin 0 :padding 0})

(defstyles footer-link-css []
  {:font-size "0.8em"})

(defn site-footer []
  [:footer {:class (concat-css global-css site-footer-css)}
   [:p {:class (footer-p-css)}
    "Copyright (C) 2019 paren-holic"
    " --- "
    [:a {:class (concat-css a-css footer-link-css)
         :href "https://gitlab.com/paren-holic/techbookfest7/website"}
     "source code of this page"]]])

(defn current-page []
  (fn []
    [:div {:class (root-css)}
     (site-bg)
     (site-header)
     (site-about)
     (site-book-list)
     (site-book-details)
     (site-footer)]))

(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "Hello shadow-cljs!!")
  (css/remove-styles!)
  (mount-root))
